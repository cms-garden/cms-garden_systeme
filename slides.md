---
title: "CMS-Garden e.V."
theme : "night"
customTheme : "custom"
parallaxBackgroundImage: images/background_garten.jpg
progress: true
controls: true
slideNumber: c/t
fragments: true
transition: concave
css: "custom.css"
margin: 0.15
revealOptions:
    width: 1280  #1200
    height: 1024  #680
    maxScale: 1.3
    minScale: 0.2
    margin: 0.15
    autoSlide: 5000  # number in millisecons or 0
    autoSlideStoppable: true
    #defaultTiming: 120
    loop: true
    progress: true
    controls: true
    slideNumber: c/t
    showSlideNumber: all  # all, speaker, print
    fragments: true
    transition: convex  # slide, slide-in, fade-out, fade, convex, concave, zoom
    css: "custom.css"
    parallaxBackgroundImage: images/background_garten.jpg
    parallaxBackgroundSize: 2600px 1024px
    parallaxBackgroundHorizontal: 100
---

![](images/cmsgarden_inverse.png)

---

# Open Source Content Management

Alle relevanten Systeme unter einem Dach

---
<!-- .slide: class="no-background" -->

<img data-src="images/CMS-Garden_CMS-Logo_contao.svg" width="1000">

---
<!-- .slide: class="no-background" -->

<img data-src="images/CMS-Garden_CMS-Logo_drupal.svg" width="1000">

---
<!-- .slide: class="no-background" -->

<img data-src="images/CMS-Garden_CMS-Logo_joomla.svg" width="1000">

---
<!-- .slide: class="no-background" -->

<img data-src="images/CMS-Garden_CMS-Logo_neos.svg" width="1000">

---
<!-- .slide: class="no-background" -->

<img data-src="images/CMS-Garden_CMS-Logo_plone.svg" width="1000">

----

# Plone

- Erste Veröffentlichung: 2001
- Sprache: Python
- Datenbank: Objektdatenbank - NoSQL (ZODB)
- Website: [plone.org](https://plone.org) / [plone.com](https://plone.com)


---
<!-- .slide: class="no-background" -->

<img data-src="images/CMS-Garden_CMS-Logo_scientificcms.svg" width="1000">

---
<!-- .slide: class="no-background" -->

<img data-src="images/CMS-Garden_CMS-Logo_typo3.svg" width="1000">

---
<!-- .slide: class="no-background" -->

<img data-src="images/CMS-Garden_CMS-Logo_umbraco.svg" width="1000">

---
<!-- .slide: class="no-background" -->

<img data-src="images/CMS-Garden_CMS-Logo_wordpress.svg" width="1000">

---
<!-- .slide: class="no-background" -->

<img data-src="images/cebit-2014-team_cropted_2019.jpg">

---
<!-- .slide: class="no-background" -->

<img data-src="images/gärtner.jpg">


---
# Auf dem laufenden bleiben

Meldet Euch auf [community.cms-garden.org](https://community.cms-garden.org) an!