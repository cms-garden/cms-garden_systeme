# Requirements

just run:

```
npm install
```

# genrate slides with reveal-md

```
reveal-md slides.md --theme night --hightlight-theme atom-one-light --css custom.css -w
```

or

```
reveal-md slides.md --theme night --hightlight-theme atom-one-light --css custom.css -w
```

# Build static version of the slides:

```
reveal-md slides.md --theme night --hightlight-theme atom-one-light --css custom.css --static public --static-dirs images
```