# Wie sich die OS-CMS zusammentaten um für OS zu kämpfen

Wie sich die Open Source CMS zusammen taten um Open Source CMS auf großer Bühne zu vertreten und sich für deren Einsatz einzusetzen.

---

# Am Anfang war die Idee

Wir wollten einen sichtbaren Stand auf der CeBIT, auf der sich mehrere freie Content Management Systeme presentieren können.

---

# Warum?

Weil dort bisher kaum freie Systeme zu finden sind.
Und dort nach wie vor viele Entscheider nach Lösungen suchen.

---

# Wie stellen wir das ganze an?

## Wir brauchen starke Partner

- Open Source park
- Wolfgang

---


---


---

# Uh, das wird nicht billig ;)

- Standfläche
- Standbau
- Technik & Werbematerial
- Unterbringung & Verpflegung

## > Geschätzte Kosten ca 100 000 EUR

---

# Ok, kurz durch atmen

Wie realisieren wir ein solches Vorhaben, mit freien Systemen?

---

# Welche CMS-Community hat diese Summen zur Verfügung?

keine ;)

---

# Geinsam sind wir stärker

Innerhalb kurzer Zeit ist es gelungen, mehr als 10 CMS-Communities für das Vorhaben zu gewinnen.

---

# Viele Schultern

## 100 000 EUR auf 10 Communities verteilt, ergibt im Schnitt 10 000 EUR

Das klingt schon eher erreichbar ;)

---

# Die starken helfen den schwachen

- Nicht jedes der Systeme hat die gleichen finanziellen Ressourcen
- Jedes System bringt sich nach seinen Möglichkeiten ein
- Teilweise wird Budget durch Arbeitsleistungen kompensiert
- Alle geben ihr bestes

---

# Aber wie werben wir das Budget ein?

![CMS-Garden-Fibel](cms-gartenfibel.png)

- Sponsoren (Organisationen: Joomla!, Drupal, TYPO3, Plone...)
- Anzeigen in der Fibel
- Einträge im Dienstleister-Register der Fibel

---

# Kein Branding eines großen Sponsors am Stand

## CMS-Garden und CMS-Communities sollen im Fokus stehen

---

# Erreichtes Budget

## ca 85 000 EUR

---

# Eingesetztes Budget am Ende

## ca 75 000

---

# Ihr seht wir waren kreativ beim Kostensparen

- Standkosten nachverhaldelt
- Übernachtungskosten reduziert
- Verpflegung selbst organisiert

---

# Jugendherberge Hildesheim

- günstig
- noch recht gut zu erreichen
- großer Gemeinschaftsraum

---

# Viel besser als Hotel

- viele Gespräche bis tief in die Nacht
- gutes Bier
- großartiges Essen, Dank Drupal Chefkoch

---

# Ein paar Zahlen

---

# Notes

Wir erzählen in diesem Vortrag was wir in den letzten Jahren alles erreicht haben und zeigen wie sich so viele unterschiedliche Systme friedlich vereinen konnten und wie es uns alle bereichert hat.
Wir stehen gerne dem Publikum für Fragen zur Verfügung sind immer daran interessiert, andere von unserer Idee zu begeistern und sie zu motivieren diese aufzugreifen.

